# -*- coding: utf-8 -*-
import os
from modules.statistic import Statistic
from modules.sorter import FreqSorterAsc, FreqSorterDesc, AlphabetSorterAsc, AlphabetSorterDesc


# Подсчитать статистику по буквам в романе Война и Мир.
# Входные параметры: файл для сканирования
# Статистику считать только для букв алфавита (см функцию .isalpha() для строк)
#
# Вывести на консоль упорядоченную статистику в виде
# +---------+----------+
# |  буква  | частота  |
# +---------+----------+
# |    А    |   77777  |
# |    Б    |   55555  |
# |   ...   |   .....  |
# |    a    |   33333  |
# |    б    |   11111  |
# |   ...   |   .....  |
# +---------+----------+
# |  итого  | 9999999  |
# +---------+----------+
#
# Упорядочивание по частоте - по убыванию. Ширину таблицы подберите по своему вкусу
#
# Требования к коду: он должен быть готовым к расширению функциональности - делать сразу на классах.
# Для этого пригодится шаблон проектирование "Шаблонный метод"
#   см https://refactoring.guru/ru/design-patterns/template-method
#   и https://gitlab.skillbox.ru/vadim_shandrinov/python_base_snippets/snippets/4


# - по частоте - по убыванию

# Зачет!
# После зачета первого этапа нужно сделать упорядочивание статистики
#  - по частоте по возрастанию
# - по алфавиту по возрастанию
#  - по алфавиту по убыванию

def main():
    input_path = 'voyna-i-mir.txt.zip'
    # TODO: Похоже, что пришли к тому с чего начинали:(
    #  Заведите классы, а ля СортировкаПоЧастотеПоВозрастанию(StatisticSorted),
    #  СортировкаПоАлфавитуПоВозрастанию(StatisticSorted) и т.д.
    #  У всех этих классов будут методы sorting_method(),
    #  которые только вызывают родительский метод и передают в него соответствующие требуемой сортировке параметры
    #  (key и reverse)
    choice_to_class = {
        1: FreqSorterDesc,
        2: FreqSorterAsc,
        3: AlphabetSorterAsc,
        4: AlphabetSorterDesc,
    }
    while True:
        choice = int(input('Если желаете упорядочить по частоте по убыванию нажмите 1\n'
                           'Если желаете упорядочить по частоте по возрастанию нажмите 2\n'
                           'Если желаете упорядочить по алфавиту по возрастанию нажмите 3\n'
                           'Если желаете упорядочить по алфавиту по убыванию нажмите 4\n'))

        if choice in choice_to_class:
            statobj = Statistic(input_path)
            statobj.collect()  # why not in init?

            sort_method = choice_to_class[choice]().sort_data
            # TODO: В зависимости от ввода пользователя создайте объект требуемого класса
            #  и у него уже вызывайте метод sorting_method()

            statobj.report_stat = sort_method(data=statobj.work_stat)
            statobj.write_on_console()
            break
        else:
            print('Вы ввели неверное число, попробуйте еще раз')


if __name__ == "__main__":
    main()
