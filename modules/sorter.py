from typing import Dict  # todo вроде в 3.9 можно без typing для ряда встроенных объектов, над почитать


class StatSorter:
    @staticmethod
    def _sort(data, key: int, reverse: bool):
        sorted_stat = sorted(data, key=lambda pair: pair[key], reverse=reverse)
        return dict(sorted_stat)


class FreqSorterAsc(StatSorter):
    def sort_data(self, data: Dict):
        return super()._sort(data=data.items(), key=1, reverse=False)


class FreqSorterDesc(StatSorter):
    def sort_data(self, data: Dict):
        return super()._sort(data=data.items(), key=1, reverse=True)


class AlphabetSorterAsc(StatSorter):
    def sort_data(self, data: Dict):
        return super()._sort(data=data.items(), key=0, reverse=False)


class AlphabetSorterDesc(StatSorter):
    def sort_data(self, data: Dict):
        return super()._sort(data=data.items(), key=0, reverse=True)
