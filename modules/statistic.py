import zipfile


class Statistic:
    def __init__(self, file_name):
        self.file_name = file_name
        self.work_stat = {}
        self.report_stat = {}

    def unzip(self):
        zfile = zipfile.ZipFile(self.file_name, 'r')
        for filename in zfile.namelist():
            zfile.extract(filename)
        self.file_name = filename

    def collect(self):
        if self.file_name.endswith('.zip'):
            self.unzip()
        with open(self.file_name, 'r', encoding='cp1251') as file:
            for line in file:
                self.collect_for_line(line=line[:-1])

    def collect_for_line(self, line):
        for char in line:
            if char.isalpha():
                if char in self.work_stat:
                    self.work_stat[char] += 1
                else:
                    self.work_stat[char] = 1

    def write_on_console(self):
        count = 0
        format_chars = ['+', '-', 'буква', 'частота', 'итого']
        head_line = f'{format_chars[0]:-<11}{format_chars[0]:-<11}{format_chars[0]}'
        words_line = f'|{format_chars[2]:^10}|{format_chars[3]:^10}|'
        print(head_line)
        print(words_line)
        print(head_line)
        for key, value in self.report_stat.items():
            count += int(value)
            line = f'|{key:^10}|{value:^10}|'
            print(line)
        print(head_line)
        count_line = f'|{format_chars[4]:^10}|{str(count):^10}|'
        print(count_line)
        print(head_line)
